package com.apps.mhassan.flickrsearch.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;

import com.apps.mhassan.flickrsearch.FlickrSearchApp;
import com.apps.mhassan.flickrsearch.R;
import com.apps.mhassan.flickrsearch.adapters.PhotoListAdapter;
import com.apps.mhassan.flickrsearch.api.FlickrApi;
import com.apps.mhassan.flickrsearch.models.FlickrPhoto;
import com.apps.mhassan.flickrsearch.presenters.DetailsActivityPresenter;

import java.util.ArrayList;

public class DetailsActivity extends AppCompatActivity {


    private Toolbar toolbar;
    private RecyclerView photoListView;
    private ProgressBar progressBar;
    private DetailsActivityPresenter detailsActivityPresenter;
    private PhotoListAdapter photoListAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        toolbar = (Toolbar) findViewById(R.id.toolbar);

        progressBar = (ProgressBar) findViewById(R.id.loading_progress_bar);
        photoListView = (RecyclerView) findViewById(R.id.photo_list);

        detailsActivityPresenter = new DetailsActivityPresenter(this);

        setupToolbar();
    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


    public void showPhotos(ArrayList<FlickrPhoto> photosData) {
        if (photoListAdapter == null) {
            FlickrApi flickrApi = FlickrSearchApp.getFlickrApi(this);
            photoListAdapter = new PhotoListAdapter(photosData, flickrApi);
            photoListAdapter.hideDetailsAction();
            if (photoListView != null) {
                photoListView.setAdapter(photoListAdapter);
                photoListView.setLayoutManager(new LinearLayoutManager(this));
            }
        } else {
            photoListAdapter.setData(photosData);
        }
    }
}
