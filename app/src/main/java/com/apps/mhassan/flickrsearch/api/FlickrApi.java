package com.apps.mhassan.flickrsearch.api;

import android.content.Context;
import android.util.Log;
import android.widget.ImageView;

import com.apps.mhassan.flickrsearch.FlickrSearchApp;
import com.apps.mhassan.flickrsearch.helpers.JsonHelper;
import com.apps.mhassan.flickrsearch.helpers.StringHelper;
import com.apps.mhassan.flickrsearch.models.FlickrPhoto;

import java.util.ArrayList;

/**
 * Created by mhassan on 2/5/16.
 */
public class FlickrApi {

    NetworkManager mNetworkManager;

    private String TAG = FlickrApi.class.getName();

    public FlickrApi(FlickrSearchApp flickrSearchApp) {
        mNetworkManager = new NetworkManager(flickrSearchApp);
    }

    public void searchPhotos(String searchText, ResultCallBack dataCallBack) {
        String url = FlickrUrlHelper.getSearchUrl(searchText);
        if (!StringHelper.isValidString(url)){
            return;
        }
        Log.i(TAG, "created url "+ url);

        getPhotosFromUrl(url, getNetworkCallBack(dataCallBack));
    }

    private ResultCallBack getNetworkCallBack(final ResultCallBack dataCallBack) {
        return new ResultCallBack() {
            @Override
            public void onSuccess(Object o) {
                if (o != null && o instanceof String){
                    String response = (String) o;
                    if (StringHelper.isValidString(response)){
                        ArrayList<FlickrPhoto> photos = JsonHelper.getPhotos(response);
                        dataCallBack.onSuccess(photos);
                        return;
                    }
                }
                Log.d(TAG, "call back error");
            }
        };
    }

    private void getPhotosFromUrl(String url, ResultCallBack resultCallBack) {
        mNetworkManager.requestStringData(url, resultCallBack);
    }

    public void fetchImage(ImageView photoImageView, String photoUrl) {
        if (mNetworkManager != null){
            mNetworkManager.downloadPhoto(photoUrl, photoImageView);
        }
    }

    public void getUserPhotos(String ownerId, ResultCallBack dataCallBack) {
        String url = FlickrUrlHelper.getUserPhotosUrl(ownerId);
        if (!StringHelper.isValidString(url)){
            return;
        }
        Log.i(TAG, "created url "+ url);

        getPhotosFromUrl(url, getNetworkCallBack(dataCallBack));
    }
}
