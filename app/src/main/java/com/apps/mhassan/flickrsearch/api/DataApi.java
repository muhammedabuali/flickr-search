package com.apps.mhassan.flickrsearch.api;

import com.apps.mhassan.flickrsearch.models.FlickrPhoto;

import java.util.ArrayList;

/**
 * Created by mhassan on 2/5/16.
 */
public class DataApi {

    private ArrayList<FlickrPhoto> searchResult = new ArrayList<>();
    private ArrayList<FlickrPhoto> userPhotos = new ArrayList<>();
    private String userId = "";

    public void setSearchResult(ArrayList<FlickrPhoto> photoList) {
        this.searchResult = photoList;
    }

    public ArrayList<FlickrPhoto> getSearchResult() {
        return searchResult;
    }

    public ArrayList<FlickrPhoto> getUserPhotos() {
        return userPhotos;
    }

    public void setUserPhotos(ArrayList<FlickrPhoto> userPhotos) {
        this.userPhotos = userPhotos;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String ownerId) {
        userId = ownerId;
    }
}
