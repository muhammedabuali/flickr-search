package com.apps.mhassan.flickrsearch.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.apps.mhassan.flickrsearch.R;
import com.apps.mhassan.flickrsearch.activities.DetailsActivity;
import com.apps.mhassan.flickrsearch.api.FlickrApi;
import com.apps.mhassan.flickrsearch.constants.FlickrConst;
import com.apps.mhassan.flickrsearch.helpers.StringHelper;
import com.apps.mhassan.flickrsearch.models.FlickrPhoto;

import java.util.ArrayList;

/**
 * Created by mhassan on 2/5/16.
 */
public class PhotoListAdapter extends RecyclerView.Adapter<PhotoListAdapter.ViewHolder>{

    private static final String TAG = PhotoListAdapter.class.getName();
    private final FlickrApi flickrApi;
    private ArrayList<FlickrPhoto> photosData;
    private boolean hideAction;

    public void hideDetailsAction() {
        hideAction = true;
    }

    public static class ViewHolder  extends RecyclerView.ViewHolder{

        private final TextView titleTextView;
        private final ImageView photoImageView;
        private final View actionImageView;

        public ViewHolder(View itemView) {
            super(itemView);
            titleTextView = (TextView) itemView.findViewById(R.id.photo_title);
            photoImageView = (ImageView) itemView.findViewById(R.id.photo_image);
            actionImageView =  itemView.findViewById(R.id.photo_details);
        }

    }

    public PhotoListAdapter(ArrayList<FlickrPhoto> photosData, FlickrApi flickrApi) {
        this.photosData = photosData;
        this.flickrApi = flickrApi;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View photoItem = inflater.inflate(R.layout.item_photo, parent, false);
        ViewHolder viewHolder = new ViewHolder(photoItem);
        return viewHolder;
    }

    @Override
    public long getItemId(int position) {
//        return super.getItemId(position);
        return position;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        if (photosData != null && photosData.size() > position){
            FlickrPhoto flickrPhoto = photosData.get(position);
            String photoTitle = flickrPhoto.getTitle();
            String photoUrl = flickrPhoto.getUrl();
            if (position == 0){
                Log.d(TAG, "photo url" + photoUrl);
            }

            if (StringHelper.isValidString(photoTitle)){
                viewHolder.titleTextView.setText(photoTitle);
            }

            viewHolder.photoImageView.setImageBitmap(null);
            if (StringHelper.isValidString(photoUrl)){
                viewHolder.photoImageView.setTag(photoUrl);
                flickrApi.fetchImage(viewHolder.photoImageView, photoUrl);
            }

            if (hideAction){
                viewHolder.actionImageView.setVisibility(View.GONE);
            }else {
                viewHolder.actionImageView.setOnClickListener(getOnClickListener(position));
            }
        }
    }

    private View.OnClickListener getOnClickListener(final int position) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context context = v.getContext();
                Intent detailsIntent = new Intent(context, DetailsActivity.class);
                detailsIntent.putExtra(FlickrConst.Photo,position);
                context.startActivity(detailsIntent);
            }
        };
    }

    @Override
    public int getItemCount() {
        if (photosData != null){
            return photosData.size();
        }
        return 0;
    }

    public void setData(ArrayList<FlickrPhoto> photosData) {
        clearPhotos();
        this.photosData = photosData;
        notifyDataSetChanged();
    }

    private void clearPhotos() {
        if (this.photosData != null){
            this.photosData.clear();
            notifyDataSetChanged();
        }
    }

}
