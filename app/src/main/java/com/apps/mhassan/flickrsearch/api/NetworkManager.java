package com.apps.mhassan.flickrsearch.api;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.widget.ImageView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apps.mhassan.flickrsearch.FlickrSearchApp;
import com.apps.mhassan.flickrsearch.helpers.StringHelper;

/**
 * Created by mhassan on 2/5/16.
 */
public class NetworkManager {
    private static final String TAG = NetworkManager.class.getName();
    private final RequestQueue mRequestQueue;
    private final Context mContext;
    private int seconds = 1000;
    private int TIMEOUT_MS = 10 * seconds;

    public NetworkManager(FlickrSearchApp flickrSearchApp) {
        mContext = flickrSearchApp;
        mRequestQueue = Volley.newRequestQueue(flickrSearchApp);
    }

    public void requestStringData(String url, ResultCallBack resultCallBack) {
        StringRequest request = new StringRequest(url,
                getResponseListener(resultCallBack), getFailListener());
        request.setShouldCache(true);
        request.setRetryPolicy(getRetryPolicy());

        mRequestQueue.add(request);
    }

    private RetryPolicy getRetryPolicy() {
        return new DefaultRetryPolicy(
                TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
    }

    private Response.ErrorListener getFailListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error != null && StringHelper.isValidString(error.getMessage())) {
                    Log.e(TAG, error.getMessage());
                } else if (error != null) {
                    error.printStackTrace();
                } else {
                    Log.e(TAG, "Volley response error");
                }
            }
        };
    }

    private Response.Listener<String> getResponseListener(final ResultCallBack resultCallBack) {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.i(TAG, "request response " + response);
                    resultCallBack.onSuccess(response);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    public void downloadPhoto(String photoUrl, ImageView photoImageView) {
        ImageRequest imageRequest = new ImageRequest(photoUrl,
                getPhotoListener(photoImageView, photoUrl),
                0, 0, ImageView.ScaleType.CENTER_CROP, null, getFailListener());
        imageRequest.setShouldCache(true);
        imageRequest.setTag(photoUrl);
        imageRequest.setRetryPolicy(getRetryPolicy());

        mRequestQueue.add(imageRequest);
    }

    private Response.Listener<Bitmap> getPhotoListener(final ImageView imageView,
                                                       final String photoUrl) {
        return new Response.Listener<Bitmap>() {
            @Override
            public void onResponse(Bitmap response) {
                if (imageView.getTag().equals(photoUrl))
                imageView.setImageBitmap(response);
            }
        };
    }
}
