package com.apps.mhassan.flickrsearch.presenters;

import android.content.Context;
import android.util.Log;

import com.apps.mhassan.flickrsearch.FlickrSearchApp;
import com.apps.mhassan.flickrsearch.activities.DetailsActivity;
import com.apps.mhassan.flickrsearch.api.DataApi;
import com.apps.mhassan.flickrsearch.api.FlickrApi;
import com.apps.mhassan.flickrsearch.api.ResultCallBack;
import com.apps.mhassan.flickrsearch.constants.FlickrConst;
import com.apps.mhassan.flickrsearch.models.FlickrPhoto;

import java.util.ArrayList;

/**
 * Created by mhassan on 2/5/16.
 */
public class DetailsActivityPresenter {

    private static final String TAG = DetailsActivityPresenter.class.getName();
    private final DetailsActivity detailsActivity;
    private FlickrApi mFlickrApi;
    private DataApi mDataApi;

    private ArrayList<FlickrPhoto> photosData = new ArrayList<>();
    private String ownerId = "";

    public DetailsActivityPresenter(DetailsActivity detailsActivity) {
        this.detailsActivity = detailsActivity;

        Context applicationContext = detailsActivity.getApplicationContext();
        if (applicationContext instanceof FlickrSearchApp) {
            FlickrSearchApp flickrSearchApp = (FlickrSearchApp) applicationContext;
            mFlickrApi = flickrSearchApp.getFlickrApi();
            mDataApi = flickrSearchApp.getDataApi();

            photosData = mDataApi.getUserPhotos();
            getUserPhotos();
        }
    }

    private void getUserPhotos() {
        FlickrPhoto clickedPhoto = getClickedPhoto();
        if (clickedPhoto != null){
            ownerId = clickedPhoto.getOwnerId();
            String oldOwner = mDataApi.getUserId();
            if (ownerId.equals(oldOwner)){
                photosData = mDataApi.getUserPhotos();
                showPhotoList();
            }else {
                if (mFlickrApi != null) {
                    mFlickrApi.getUserPhotos(ownerId, getSearchCallBack());
                }
            }
        }
    }

    private ResultCallBack getSearchCallBack() {
        return new ResultCallBack() {
            @Override
            public void onSuccess(Object o) {
                if (o != null && o instanceof ArrayList) {
                    DetailsActivityPresenter.this.photosData = (ArrayList<FlickrPhoto>) o;
                    mDataApi.setUserPhotos(photosData);
                    mDataApi.setUserId(ownerId);
                    showPhotoList();
                }
            }
        };
    }

    private void showPhotoList() {
        if (photosData == null) {
            return;
        }
        Log.i(TAG, "photos count " + photosData.size());
        detailsActivity.showPhotos(photosData);
    }

    private FlickrPhoto getClickedPhoto() {
        int photoPosition = detailsActivity.getIntent().getIntExtra(FlickrConst.Photo, -1);
        if (photoPosition > -1){
            ArrayList<FlickrPhoto> searchResult = mDataApi.getSearchResult();
            if (searchResult != null && searchResult.size() > photoPosition){
                FlickrPhoto flickrPhoto = searchResult.get(photoPosition);
                return flickrPhoto;
            }
        }
        return null;
    }
}
