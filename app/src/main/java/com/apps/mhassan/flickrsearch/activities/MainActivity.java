package com.apps.mhassan.flickrsearch.activities;

import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.apps.mhassan.flickrsearch.FlickrSearchApp;
import com.apps.mhassan.flickrsearch.R;
import com.apps.mhassan.flickrsearch.adapters.PhotoListAdapter;
import com.apps.mhassan.flickrsearch.api.FlickrApi;
import com.apps.mhassan.flickrsearch.models.FlickrPhoto;
import com.apps.mhassan.flickrsearch.presenters.MainActivityPresenter;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getName();
    private MainActivityPresenter mainActivityPresenter;
    //views
    private ProgressBar progressBar;
    private RecyclerView recylerView;
    private PhotoListAdapter photoListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.title_activity_main));
        setSupportActionBar(toolbar);

        progressBar = (ProgressBar) findViewById(R.id.loading_progress_bar);
        recylerView = (RecyclerView) findViewById(R.id.photo_list);

        mainActivityPresenter = new MainActivityPresenter(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(getQueryListener(searchView));

        return true;
    }


    private SearchView.OnQueryTextListener getQueryListener(final SearchView searchView) {
        return new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchView.clearFocus();
//                setBusyIndicatorVisible(true);

                mainActivityPresenter.searchFor(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }

        };
    }

    private void setBusyIndicatorVisible(boolean visible) {
        if (visible) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    public void showPhotos(ArrayList<FlickrPhoto> photosData) {
        if (photoListAdapter == null) {
            FlickrApi flickrApi = FlickrSearchApp.getFlickrApi(this);
            photoListAdapter = new PhotoListAdapter(photosData, flickrApi);
            if (recylerView != null) {
                recylerView.setAdapter(photoListAdapter);
                recylerView.setLayoutManager(new LinearLayoutManager(this));
            }
        } else {
            photoListAdapter.setData(photosData);
        }
    }
}
