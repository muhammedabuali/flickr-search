package com.apps.mhassan.flickrsearch.api;

/**
 * Created by mhassan on 2/5/16.
 */
public interface ResultCallBack {
    void onSuccess(Object o);
}
