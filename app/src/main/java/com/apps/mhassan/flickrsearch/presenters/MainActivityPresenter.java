package com.apps.mhassan.flickrsearch.presenters;

import android.content.Context;
import android.util.Log;

import com.apps.mhassan.flickrsearch.FlickrSearchApp;
import com.apps.mhassan.flickrsearch.activities.MainActivity;
import com.apps.mhassan.flickrsearch.api.DataApi;
import com.apps.mhassan.flickrsearch.api.ResultCallBack;
import com.apps.mhassan.flickrsearch.helpers.StringHelper;
import com.apps.mhassan.flickrsearch.api.FlickrApi;
import com.apps.mhassan.flickrsearch.models.FlickrPhoto;

import java.util.ArrayList;

/**
 * Created by mhassan on 2/5/16.
 */
public class MainActivityPresenter {

    private static final String TAG = MainActivityPresenter.class.getName();
    private final MainActivity mainActivity;
    private Context mContext;
    private FlickrApi mFlickrApi;
    private DataApi mDataApi;
    private ArrayList<FlickrPhoto> photosData = new ArrayList<>();

    public MainActivityPresenter(MainActivity mainActivity) {
        this.mainActivity = mainActivity;

        Context applicationContext = mainActivity.getApplicationContext();
        if (applicationContext instanceof FlickrSearchApp) {
            FlickrSearchApp flickrSearchApp = (FlickrSearchApp) applicationContext;
            mFlickrApi = flickrSearchApp.getFlickrApi();
            mDataApi = flickrSearchApp.getDataApi();

            photosData = mDataApi.getSearchResult();
            showPhotoList();
        }
    }

    public void searchFor(String searchText) {
        if (!StringHelper.isValidString(searchText)) {
            return;
        }
        Log.i(TAG, "searching for : " + searchText);
        if (mFlickrApi != null) {
            mFlickrApi.searchPhotos(searchText, getSearchCallBack());
        }
    }

    private ResultCallBack getSearchCallBack() {
        return new ResultCallBack() {
            @Override
            public void onSuccess(Object o) {
                if (o != null && o instanceof ArrayList) {
                    MainActivityPresenter.this.photosData = (ArrayList<FlickrPhoto>) o;
                    mDataApi.setSearchResult(photosData);
                    showPhotoList();
                }
            }
        };
    }

    private void showPhotoList() {
        if (photosData == null) {
            return;
        }
        Log.i(TAG, "photos count " + photosData.size());
        mainActivity.showPhotos(photosData);
    }
}
