package com.apps.mhassan.flickrsearch.constants;

/**
 * Created by mhassan on 2/5/16.
 */
public class FlickrConst {
    public static final String Photos = "photos";
    public static final String Photo = "photo";
}
