package com.apps.mhassan.flickrsearch.api;

import android.net.Uri;

import com.apps.mhassan.flickrsearch.models.FlickrPhoto;

/**
 * Created by mhassan on 2/5/16.
 */

public class FlickrUrlHelper {

    private static String Url_Scheme = "https";
    private static String Base_Url = "api.flickr.com";
    private static String Extended_Path = "services/rest/";

    private static String API_Key_String = "api_key";
    private static final String API_Key = "d8527026abebb4087e0214015ef92f81";

    private static String Method_String = "method";
    private static String Method_Search = "flickr.photos.search";

    private static String Text_String = "text";

    private static String Sort_String = "sort";
    private static String Sort_Value = "relevance";

    private static String Safe_Search_String = "safe_search";
    private static String Safe_Search_Value = "1";

    private static String Format_String = "format";
    private static String Format_Value = "json";

    private static String Media_String = "media";
    private static String Media_Value = "photos";

    private static String Farm_String = "farm";
    private static String Photo_Base_Url = ".staticflickr.com";
    private static String Separator ="_";
    private static String Default_Photo_Size = "m";
    private static String Photo_Format =".jpg";
    private static String User_Id_String = "user_id";


    public static String getSearchUrl(String searchText) {
        Uri.Builder builder = new Uri.Builder();
        builder.scheme(Url_Scheme)
                .authority(Base_Url)
                .appendEncodedPath(Extended_Path)
                .appendQueryParameter(API_Key_String, API_Key)
                .appendQueryParameter(Format_String, Format_Value)
                .appendQueryParameter(Method_String, Method_Search)
                .appendQueryParameter(Safe_Search_String, Safe_Search_Value)
                .appendQueryParameter(Text_String, searchText)
                .appendQueryParameter(Media_String, Media_Value)
                .appendQueryParameter(Sort_String, Sort_Value);
        String url = "";
        try {
            url = builder.build().toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return url;
    }

    public static String getPhotoUrl(FlickrPhoto flickrPhoto) {
        //https://farm{farm-id}.staticflickr.com/{server-id}/{id}_{secret}_[mstzb].jpg
        String serverId = flickrPhoto.getServerId();
        String photoId = flickrPhoto.getPhotoId();
        String photoSecret = flickrPhoto.getSecret();
        String longName= photoId
                + Separator + photoSecret
                + Separator + Default_Photo_Size + Photo_Format;

        String photoFarm = Farm_String + flickrPhoto.getFarmId();
        String authority = photoFarm + Photo_Base_Url;

        Uri.Builder builder = new Uri.Builder();
        builder.scheme(Url_Scheme)
                .authority(authority)
                .appendEncodedPath(serverId)
                .appendEncodedPath(longName);

        String url = "";
        try {
            url = builder.build().toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return url;
    }

    public static String getUserPhotosUrl(String ownerId) {
        Uri.Builder builder = new Uri.Builder();
        builder.scheme(Url_Scheme)
                .authority(Base_Url)
                .appendEncodedPath(Extended_Path)
                .appendQueryParameter(API_Key_String, API_Key)
                .appendQueryParameter(Format_String, Format_Value)
                .appendQueryParameter(Method_String, Method_Search)
                .appendQueryParameter(Safe_Search_String, Safe_Search_Value)
                .appendQueryParameter(User_Id_String, ownerId)
                .appendQueryParameter(Media_String, Media_Value)
                .appendQueryParameter(Sort_String, Sort_Value);
        String url = "";
        try {
            url = builder.build().toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return url;
    }
}
