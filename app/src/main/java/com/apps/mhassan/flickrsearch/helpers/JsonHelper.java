package com.apps.mhassan.flickrsearch.helpers;

import com.apps.mhassan.flickrsearch.constants.FlickrConst;
import com.apps.mhassan.flickrsearch.models.FlickrPhoto;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by mhassan on 2/5/16.
 */
public class JsonHelper {

    public static final String Start_Parenthesis = "{";
    public static final String End_Parenthesis = "}";
    ;

    public static ArrayList<FlickrPhoto> getPhotos(String data) {
        if (!StringHelper.isValidString(data)) {
            return null;
        }

        String flickrJsonString = getFlickrJsonString(data);
        JSONArray flickrPhotosJsonArray = getFlickrPhotosArray(flickrJsonString);
        if (flickrPhotosJsonArray == null) {
            return new ArrayList<>();
        }
        ArrayList<FlickrPhoto> flickrPhotos = getFlickrPhotosFromJson(flickrPhotosJsonArray);
        return flickrPhotos;
    }

    private static ArrayList<FlickrPhoto> getFlickrPhotosFromJson(JSONArray flickrPhotosJsonArray) {
        ArrayList<FlickrPhoto> flickrPhotos = new ArrayList<>();
        for (int i = 0; i < flickrPhotosJsonArray.length(); i++) {
            try {
                JSONObject photoJSONObject = flickrPhotosJsonArray.getJSONObject(i);
                FlickrPhoto flickrPhoto = new FlickrPhoto(photoJSONObject);
                flickrPhotos.add(flickrPhoto);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return flickrPhotos;
    }

    private static JSONArray getFlickrPhotosArray(String flickrJsonString) {
        try {
            JSONObject flickrJsonObject = new JSONObject(flickrJsonString);
            JSONObject photosObject = (JSONObject) flickrJsonObject.get(FlickrConst.Photos);
            JSONArray photosJSONArray = photosObject.getJSONArray(FlickrConst.Photo);
            return photosJSONArray;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private static String getFlickrJsonString(String data) {
        String out = "";
        int startIndex = data.indexOf(Start_Parenthesis);
        if (startIndex == -1) {
            return out;
        }
        int endIndex = data.lastIndexOf(End_Parenthesis);
        if (endIndex == -1) {
            return out;
        }
        out = data.substring(startIndex, endIndex + 1);
        return out;
    }

    public static String getStringAttribute(JSONObject jsonObject, String attributeName) {
        try {
            if (jsonObject != null) {
                if (jsonObject.has(attributeName)) {
                    Object o = jsonObject.get(attributeName);
                    if (o != null){
                        return o.toString();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }
}
