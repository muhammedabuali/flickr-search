package com.apps.mhassan.flickrsearch.models;

import com.apps.mhassan.flickrsearch.api.FlickrUrlHelper;
import com.apps.mhassan.flickrsearch.helpers.JsonHelper;

import org.json.JSONObject;

/**
 * Created by mhassan on 2/5/16.
 */
public class FlickrPhoto {
    private final JSONObject jsonObject;

    public FlickrPhoto(JSONObject photoJSONObject) {
        this.jsonObject = photoJSONObject;
    }

    public String getTitle() {
        return JsonHelper.getStringAttribute(jsonObject, Attribute.Title);
    }

    public String getUrl() {
        return FlickrUrlHelper.getPhotoUrl(this);
    }

    public String getServerId() {
        return JsonHelper.getStringAttribute(jsonObject, Attribute.Server_Id);
    }

    public String getPhotoId() {
        return JsonHelper.getStringAttribute(jsonObject, Attribute.Photo_Id);
    }

    public String getSecret() {
        return JsonHelper.getStringAttribute(jsonObject, Attribute.Photo_Secret);
    }

    public String getFarmId() {
        return JsonHelper.getStringAttribute(jsonObject, Attribute.Farm_Id);
    }

    public String getOwnerId() {
        return JsonHelper.getStringAttribute(jsonObject, Attribute.Owner);
    }

    private static class Attribute {
        public static String Title = "title";
        public static String Server_Id = "server";
        public static String Photo_Id = "id";
        public static String Photo_Secret = "secret";
        public static String Farm_Id = "farm";
        public static String Owner = "owner";
    }
}
