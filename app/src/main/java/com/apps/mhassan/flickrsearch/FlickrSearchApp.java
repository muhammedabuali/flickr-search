package com.apps.mhassan.flickrsearch;

import android.app.Application;
import android.content.Context;

import com.apps.mhassan.flickrsearch.api.DataApi;
import com.apps.mhassan.flickrsearch.api.FlickrApi;

/**
 * Created by mhassan on 2/5/16.
 */
public class FlickrSearchApp extends Application {

    private FlickrApi appflickrApi;
    private DataApi appDataApi;

    @Override
    public void onCreate() {
        super.onCreate();

        appflickrApi = new FlickrApi(this);
        appDataApi = new DataApi();
    }

    public FlickrApi getFlickrApi() {
        return appflickrApi;
    }

    public static FlickrApi getFlickrApi(Context context) {
        Context applicationContext = context.getApplicationContext();
        if (applicationContext instanceof FlickrSearchApp) {
            FlickrSearchApp flickrSearchApp = (FlickrSearchApp) applicationContext;
            FlickrApi flickrApi = flickrSearchApp.getFlickrApi();
            return flickrApi;
        }
        return null;
    }

    public DataApi getDataApi() {
        return appDataApi;
    }
}
